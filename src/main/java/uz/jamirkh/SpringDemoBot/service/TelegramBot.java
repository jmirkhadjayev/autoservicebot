package uz.jamirkh.SpringDemoBot.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.jamirkh.SpringDemoBot.config.BotConfig;

@Component

public class TelegramBot extends TelegramLongPollingBot {
    final BotConfig config;
    private final UserService userService;

    @Autowired
    public TelegramBot(BotConfig config, UserService userService) {
        this.config = config;
        this.userService = userService;
    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    @Override
    public String getBotToken() {
        return config.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()) {

            Message message = update.getMessage();

            if (message.hasText()) {

            } else if (message.hasContact()) {

            }

            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();

            switch (messageText) {
                case "/start":

                    startCommandReceived(chatId, update.getMessage().getChat()
                            .getFirstName());
                    break;
                default:
                    sendMessage(chatId, "Uzr buyrug'ingiz qabul qilinmadi");
            }
        }
    }

    private void startCommandReceived(long chatId, String name) {
        String answer = "Assalamu alaikum, " + name + " bizning botimizga hush kelibsiz!";
        sendMessage(chatId, answer);
    }

    @SneakyThrows
    private void sendMessage(long chatId, String textToSend) {

        execute(SendMessage.builder()
                .chatId(String.valueOf(chatId))
                .text(textToSend)
                .build());

    }
}
