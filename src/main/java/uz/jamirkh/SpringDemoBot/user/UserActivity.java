package uz.jamirkh.SpringDemoBot.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserActivity {
    private Long chatId;
    private int round;
    private User user;
}
