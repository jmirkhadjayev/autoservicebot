package uz.jamirkh.SpringDemoBot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jamirkh.SpringDemoBot.user.User;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByChatId(Long chatId);
}
