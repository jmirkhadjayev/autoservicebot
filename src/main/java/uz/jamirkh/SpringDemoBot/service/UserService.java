package uz.jamirkh.SpringDemoBot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Contact;
import uz.jamirkh.SpringDemoBot.repository.UserRepository;
import uz.jamirkh.SpringDemoBot.user.User;
import uz.jamirkh.SpringDemoBot.user.UserActivity;
import uz.jamirkh.SpringDemoBot.user.UserDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;


    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User get(Long chatId) {

        Optional<User> optionalUser = userRepository.findByChatId(chatId);

        if (optionalUser.isPresent()) return optionalUser.get();

        User user = User.builder().chatId(chatId).build();

        return userRepository.save(user);

    }


    public User update(User currentUser, Contact contact) {

        String firstName = contact.getFirstName();
        String lastName = contact.getLastName();
        String phoneNumber = contact.getPhoneNumber();

        Optional<User> optionalUser = userRepository.findById(currentUser.getId());

        if (optionalUser.isEmpty()) throw new RuntimeException("User not found");

        User user = optionalUser.get();
        user.setFullName(new UserDetails(firstName, lastName));
        user.setPhoneNumber(phoneNumber);

        return userRepository.save(user);
    }


    public Map<Long, UserActivity> userActivityMap() {

        Map<Long, UserActivity> userActivityMap = new HashMap<>();

        for (User user : getAll()) {
            UserActivity userActivity = new UserActivity();
            userActivity.setUser(user);
            userActivityMap.put(user.getChatId(), userActivity);
        }

        return userActivityMap;
    }


    private List<User> getAll() {

        return userRepository.findAll();

    }


}
