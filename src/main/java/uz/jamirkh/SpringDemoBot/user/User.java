package uz.jamirkh.SpringDemoBot.user;

import lombok.*;
import uz.jamirkh.SpringDemoBot.entity.absEntity.AbsEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

@Builder
@Entity(name = "users")
public class User extends AbsEntity {

    private String phoneNumber;
    private String username;
    private Long chatId;

    @Enumerated(EnumType.STRING)
    private Role role;

    private int round;

    @Embedded
    private UserDetails fullName;

}